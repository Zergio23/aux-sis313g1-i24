const play = document.getElementById('circle')
const video = document.getElementById('video')

play.addEventListener('click', () => {
    if (video.paused) {
        play.style.visibility = "hidden"
        video.play()
    } else {
        play.style.visibility = "visible"
        video.pause()
    }
})
video.addEventListener('click', () => {
    if (video.paused) {
        play.style.visibility = "hidden"
        video.play()
    } else {
        play.style.visibility = "visible"
        video.pause()
    }
})