import { Avatar, Box, Card, Typography } from "@mui/material"

interface profileProps {
    avatarUrl: string,
    name: string,
    description: string
}

const Profile = (props: profileProps) => {
    const {avatarUrl, name, description} = props;
    return(
        <Card sx={cardStyle}>
            <Box sx={boxStyle}>
                <Avatar sx={avatarStyle} alt="profile-picture" src={avatarUrl}></Avatar>
                <Typography variant="h4">{name}</Typography>
            </Box>
            <Typography variant="body1">{description}</Typography>
        </Card>
    )
}

// Creo una constante donde almaceno los estilos para mi componente
const boxStyle = {
    display: "flex",
    alignItems: "center",
    marginBottom: "20px"
}
const cardStyle = {
    padding: "30px",
    maxWidth: "700px"
}
const avatarStyle = {
    width: "60px",
    height: "60px",
    marginRight: "20px"
}

export default Profile