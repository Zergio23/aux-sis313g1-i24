import Image from "next/image";
import styles from "./page.module.css";
import Profile from "@/components/Profile";
import { Box, LinearProgress, Typography } from "@mui/material";

export default function Home() {
  return (
    <main className="main-container">
      <Typography variant="h3" sx={{textAlign: "center", marginTop: "20px"}}>Lab 4 - 04/june/2024</Typography>
      <LinearProgress sx={{margin: "25px 0"}}/>
      <Box className="profile-container">
        <Profile avatarUrl="./img/senshi-profile.jpg" name="Univ. Sergio Moises Apaza Caballero" description="Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur tempore nostrum consectetur architecto hic veritatis doloremque provident sint, doloribus quisquam sit tenetur perferendis reiciendis ab cumque minima impedit recusandae eos eius temporibus totam ullam optio at modi. Illo, vel tempora."/>
        <Profile avatarUrl="./img/senshi-profile.jpg" name="Univ. Sergio Moises Apaza Caballero" description="Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur tempore nostrum consectetur architecto hic veritatis doloremque provident sint, doloribus quisquam sit tenetur perferendis reiciendis ab cumque minima impedit recusandae eos eius temporibus totam ullam optio at modi. Illo, vel tempora."/>
        <Profile avatarUrl="./img/senshi-profile.jpg" name="Univ. Sergio Moises Apaza Caballero" description="Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur tempore nostrum consectetur architecto hic veritatis doloremque provident sint, doloribus quisquam sit tenetur perferendis reiciendis ab cumque minima impedit recusandae eos eius temporibus totam ullam optio at modi. Illo, vel tempora."/>
        <Profile avatarUrl="./img/senshi-profile.jpg" name="Univ. Sergio Moises Apaza Caballero" description="Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur tempore nostrum consectetur architecto hic veritatis doloremque provident sint, doloribus quisquam sit tenetur perferendis reiciendis ab cumque minima impedit recusandae eos eius temporibus totam ullam optio at modi. Illo, vel tempora."/>
        <Profile avatarUrl="./img/senshi-profile.jpg" name="Univ. Sergio Moises Apaza Caballero" description="Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur tempore nostrum consectetur architecto hic veritatis doloremque provident sint, doloribus quisquam sit tenetur perferendis reiciendis ab cumque minima impedit recusandae eos eius temporibus totam ullam optio at modi. Illo, vel tempora."/>
      </Box>
    </main>
  );
}
