# TOP
## SUMARIO
1. [PRÁCTICA 3](#práctica-3)
2. [PRÁCTICA 4](#práctica-4)
3. [PRÁCTICA 5](#práctica-5)
4. [PRÁCTICA 6](#práctica-6)
5. [PRÁCTICA 7](#práctica-7)
6. [PRÁCTICA 8](#práctica-8)
7. [PRÁCTICA 9](#práctica-9)
8. [PRÁCTICA 11](#práctica-11)
9. [PRÁCTICA 12](#práctica-12)
10. [PRÁCTICA 13](#práctica-13)
11. [PRÁCTICA 14](#práctica-14)
12. [PRÁCTICA 15](#práctica-15)
12. [PRÁCTICA 16](#práctica-16)

## PRÁCTICA 3
    1. @mayerlycoro
    2. @Sharick_Karla_Chungara_Paco
    3. @gaboprogra
    4. @Will_MG
    5. @mariabetzabeortegasoto

## PRÁCTICA 4
    1. @Sharick_Karla_Chungara_Paco
    2. @mayerlycoro
    3. @samiraarancibiavera
    4. @Alex_Sander_Lopez_Orcko
    5. @Liz_Mariela_Perez_Gervacio
    MENCIÓN HONROSA: @Will_MG

## PRÁCTICA 5
    1. @samiraarancibiavera
    2. @Sharick_Karla_Chungara_Paco
    3. @mariabetzabeortegasoto
    4. @cruzcoaquirabrayan
    5. @mayerlycoro

## PRÁCTICA 6
    1. @Alex_04
    2. @aszlangel56
    3. @gaboprogra
    4. @Will_MG
    5. @JhimyJulianClemente

## PRÁCTICA 7
    1. @mayerlycoro
    2. @Jh0vanny
    3. @Sharick_Karla_Chungara_Paco
    4. @samiraarancibiavera
    5. @JASELALEXANDERAYMATITICAYO

## PRÁCTICA 8
    1. @mayerlycoro
    2. @Sharick_Karla_Chungara_Paco
    3. @Alex_04
    4. @aszlangel56
    5. @salazarhuanacolisedariane

## PRÁCTICA 9
    1. @Alex_04
    2. @Erick_Vargas
    3. @mariabetzabeortegasoto
    4. @JhimyJulianClemente
    5. @samiraarancibiavera

## PRÁCTICA 11
    1. @Sharick_Karla_Chungara_Paco
    2. @salazarhuanacolisedariane
    3. @mayerlycoro
    4. @DannerJoseQuispeHuallpa
    5. @Alex_04

## PRÁCTICA 12
    1. @aszlangel56
    2. @mayerlycoro
    3. @samiraarancibiavera
    4. @Erick_Vargas
    5. @JhimyJulianClemente

## PRÁCTICA 13
    1. @salazarhuanacolisedariane
    2. @Sharick_Karla_Chungara_Paco
    3. @mariabetzabeortegasoto
    4. @Alex_04
    5. @Liz_Mariela_Perez_Gervacio

## PRÁCTICA 14
    1. @Sharick_Karla_Chungara_Paco
    2. @salazarhuanacolisedariane
    3. @mariabetzabeortegasoto
    4. @JhimyJulianClemente
    5. @DannerJoseQuispeHuallpa

## PRÁCTICA 15
    1. @Sharick_Karla_Chungara_Paco
    2. @quispeogisell
    3. @Alex_04
    4. @mayerlycoro
    5. @JhimyJulianClemente

## PRÁCTICA 16
    1. @JhimyJulianClemente
    2. @Sharick_Karla_Chungara_Paco
    3. @JhosueFlorentinoCruzZuna