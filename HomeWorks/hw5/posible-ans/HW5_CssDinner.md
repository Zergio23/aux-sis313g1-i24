
# **PRÁCTICA N°5** 📓

**DATOS DEL ESTUDIANTE**

👨‍🎓**Docente:** Ing. Jose David Mamani Figueroa 

👨‍💻**Auxiliar:** Univ. Sergio Moises Apaza Caballero 

🎨**Materia:** DISEÑO Y PROGRAMACIÓN GRÁFICA 

📚**Siglas:** SIS-313 G1 

🗓️**Fecha:** 04/04/2024 

## **Preguntas**:

- [x] **1) Complete los primeros 5 niveles y los últimos 10 igualmente, mostrar una captura completa de la pantalla con el nivel vencido. En cada nivel se debe mostrar la siguiente lista (que aparece en la parte derecha de la página):**
# **CSS Diner** 🍽️
El juego nos permite jugar con los objetos/elementos mientras aprendemos a conectar adecuadamente desde nuestro **CSS** los elemnetos del **HTML** por medio de los selectores.

![css diner](https://github.com/sharick-14/imgenes-prc4/assets/164090219/8ee600c2-a013-4afc-8797-769ccf7394d5)

* ## 🌟**LEVEL 1** 
Sleccionar los platos, gracias al uso del selector de las etiquetas, mandando a llamar la etiqueta solo se escribe la palabra **plate** porque solo necesitamos llamar eso en especifico, ya que es una determinada etiqueta.

![level1](https://github.com/sharick-14/imgenes-prc4/assets/164090219/80cba642-28d7-40a4-ae4d-0e48df7e8a5d)

    NEXT LEVEL ⏭️

* ## 🌟**LEVEL 2** 🍱
Selecciona las cajas bento, donde solo debemos de llamar a **bento**, se repite lo mismo que el ejercicio anterior.

![level2](https://github.com/sharick-14/imgenes-prc4/assets/164090219/42b622ea-ca31-4a99-9317-9b3d2abdcbd7)

 
    NEXT LEVEL ⏭️
 
* ## 🌟**LEVEL 3** 
Ahora debemos de seleccionar el plato *fancy*, ya que es algo en especifico, por que eso esta con el atributo id, para eso lo que debemos de hacer el mas facil es solo poner el id, que es fancy **(#fancy)**.

![level3](https://github.com/sharick-14/imgenes-prc4/assets/164090219/2408f65c-c626-4284-bbab-8e8c1b03088d)

    NEXT LEVEL ⏭️


* ## 🌟**LEVEL 4** 
Debemos de seleccionar una manzana en el plato, pero ahora son selectores descendientes, osea un elemento dentro de otro elemento **(padre -hijo)**.
Lo que debmos de hacer es hacer lo siguiente, sabesmo que la manzana esta dentro del plato, asi que debemos de llamar al plato y luego a al manzana, de la siguiente manera:

**plate apple**

![level4](https://github.com/sharick-14/imgenes-prc4/assets/164090219/648445f6-93aa-4d5e-9de8-a41964056318)

    NEXT LEVEL ⏭️


* ## 🌟**LEVEL 5** 
Combinar un descendiente y selector de id, solo debemos de llamar al pepinillo que estaba en el plato fancy y para por realizarlo hacemos lo siguiente: 

**#fancy pickle**

![level5](https://github.com/sharick-14/imgenes-prc4/assets/164090219/3f1c42df-ccf6-42db-8014-62e6a50155da)

    NEXT LEVEL ⏭️

* ## 🌟**LEVEL 22**
Para este nivel, debemos de selccionar cada segundo plato apartir del tercer elemento.

**plate:nth-of-type(2n+3)**

![level22](https://github.com/sharick-14/imgenes-prc4/assets/164090219/afde3169-6f75-4642-bc13-aab8b6be5c94)


    NEXT LEVEL ⏭️

* ## 🌟**LEVEL 23**
Seleccionar un tipo de selector

**plate apple:only-of-type**

![level23](https://github.com/sharick-14/imgenes-prc4/assets/164090219/2b2223a5-58ed-4904-88d1-5eb3da365d64)

    NEXT LEVEL ⏭️
* ## 🌟**LEVEL 24**
Seleccionar el ultimo de su tipo de la manzana y de la naranja.

**orange:last-of-type, apple:last-of-type**

![level24](https://github.com/sharick-14/imgenes-prc4/assets/164090219/b44ecc08-8ee1-407a-94cd-cda4750a4aa8)

    NEXT LEVEL ⏭️
* ## 🌟**LEVEL 25**
Seleccionar los selectores vacios, en especifico de los bento

**bento:empty**

![level25](https://github.com/sharick-14/imgenes-prc4/assets/164090219/645bb8e4-0846-407f-8dce-4f8773a79363)

    NEXT LEVEL ⏭️

* ## 🌟**LEVEL 26**
Selecciona las manzanas grandes, cualquiera cosa que no sea apple, de tipo etiqueta small.

**apple:not(.small)**

![level26](https://github.com/sharick-14/imgenes-prc4/assets/164090219/df36e5cd-92ac-4168-a716-964ba5e09446)

    NEXT LEVEL ⏭️
* ## 🌟**LEVEL 27** 
Seleccionar todos los elementos que tengan el atributo for.

***[for]**

![level27](https://github.com/sharick-14/imgenes-prc4/assets/164090219/d8f1f6c2-b313-4bb6-a8cd-2caa5c389309)

    NEXT LEVEL ⏭️


* ## 🌟**LEVEL 28** 
Debemos de seleccionar los platos para alguien, en este caso solo los que tengan plato.

**plate[for]**

![level28](https://github.com/sharick-14/imgenes-prc4/assets/164090219/a5cc4fd4-9a1e-4ac7-95c6-369437342727)

    NEXT LEVEL ⏭️

* ## 🌟**LEVEL 29** 
Debemos de seleccioanr solo el que tenga por nombre Vitaly, para eso dentro del atributo, aplicamos lo siguiente:

**[for="Vitaly"]**

![level29](https://github.com/sharick-14/imgenes-prc4/assets/164090219/56757bd0-c9ed-4dc9-8789-f5f29c3016fa)

    NEXT LEVEL ⏭️

* ## 🌟**LEVEL 30** 
Que seleccione todo lo que empiza con la (Sa), de la siguinete manera:

**[for^="Sa"]**

*El ^ quiere decir (empieza)*

![level30](https://github.com/sharick-14/imgenes-prc4/assets/164090219/82b60e88-bcad-4842-b969-32c8d58cdb8e)

    NEXT LEVEL ⏭️

* ## 🌟**LEVEL 31**
Selecciona los elementos con un valor de atributo que terminen en *(to)* o tambien *(ato)* ya que funciona de ambas maneras.


**[for$="to"]**

*El $ significa(termina)*

![level31](https://github.com/sharick-14/imgenes-prc4/assets/164090219/640587ce-a71a-4e6b-a414-0fb80be255aa)


    NEXT LEVEL ⏭️


* ## 🌟**LEVEL 32** 
Selecciona todos los almuerzos, cuyos nombres contengan el caracter (o) pero tambien funciona si ponemos (obb):

[for*="o"]

*El * selecciona cualquier cosa, pero sin importar en donde este ubicado, pero que contenga un determinado valor, en este caso o.*

![level32](https://github.com/sharick-14/imgenes-prc4/assets/164090219/cc954a05-f657-4c22-8517-ca877d030907)


    FINISHED 🏆

![level complete](https://github.com/sharick-14/imgenes-prc4/assets/164090219/60011482-37c0-4844-b513-a7924ddc84c9)


## ***LISTA COMPLETA DE TODOS LOS NIVELES***🎮:

![lista](https://github.com/sharick-14/imgenes-prc4/assets/164090219/a955114a-f074-43e7-9050-2c6638df46f3)

![lista2](https://github.com/sharick-14/imgenes-prc4/assets/164090219/932184b0-0d73-4620-b407-41d68b40ce1c)

## **TAREA FINALIZADA** :)