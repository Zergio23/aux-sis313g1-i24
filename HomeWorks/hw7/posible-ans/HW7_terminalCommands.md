
# **PRÁCTICA N° 7** 📓
**DATOS DEL ESTUDIANTE**

👨‍🎓**Docente:** Ing. Jose David Mamani Figueroa 

👨‍💻**Auxiliar:** Univ. Sergio Moises Apaza Caballero 

🎨**Materia:** DISEÑO Y PROGRAMACIÓN GRÁFICA 

📚**Siglas:** SIS-313 G1 

🗓️**Fecha:** 23/04/2024

# PREGUNTAS:
👉 **1. Realice un video explicando lo siguiente:**
* a. La creación de una carpeta en el disco local C:, mediante comandos desde la terminal

* b. La forma de cambiar de directorios utilizando comandos

**NOTA: El video solo deberá explicar la creación de UNA SOLA carpetas**
 
        LINK DEL VIDEO ⏭️ HAGA CLICK EN EL LINK 

https://drive.google.com/file/d/1VwMpCY_Sr55E90NGbeYz79HB4dJ_W13N/view?usp=sharing

👉 **2. Adjunte capturas de pantalla del mismo proceso, tres veces más.**

Creación de una carpeta en el disco local C:, mediante comandos desde la termina
Para esto se utliza el comando **md**

![comando md](https://github.com/sharick-14/imgenes-prc4/assets/164090219/b96f076d-3372-48e7-99a6-813eaefc3a17)

Cambiar de directorios utilizando comandos
Para cambiar (en este caso retroceder) de direcciones utilizamos 
cd ..    Y para poder direccionarnos utlizamos un cd 

![comando-cd](https://github.com/sharick-14/imgenes-prc4/assets/164090219/6a1ad6ab-f14f-4df6-b5c6-ec8c3b663d5d)

👉 **3. Que comandos se utilizó en las anteriores preguntas.
Los comandos que se ultizaron fueron el:**
* ## md   
* ##  cd  
* ## cd ..
Estos tres comandos utlizamos en las preguntas anteriores.

👉 **4. Explique 5 comandos para ser utilizados en la terminal y su forma de usarse en Windows (Terminal o Powershell) y en Linux (bash).**

Los comandos que se utlizan en Windows (Powershell) son:
* ## **md** Creacion de carpetas o directorio
* ## **cd** Este es el comando más importante. Sirve para cambiar de directorio.Se utiliza de la siguiente manera cd <RutaDirectorio> para ir al directorio o carpeta específica.
* ## **cd ..** Nos sirve para salir de una carpeta y volver al nivel superior (o inicio) o a la carpeta donde estabamos. 
* ## **dir** Su función es que lista el contenido del directorio o carpeta en la que nos encontramos, donde nos va a mostrar todas los archivos que estan y las subcarpetas, nos sirve para verificar si el archivo esta existe y esta ahi.
* ## **cls**  Este comadno nos sirve para porder limpiar la ventada de la consola o en todo caso borrar todo lo que esta en la consola , en resumen, todo aparece como si hubieramos havierto una nueva consola, ya que estar sin nada, todo se habra eliminado.
    TAREA COMPLETA 🌟🌟🌟🌟🌟