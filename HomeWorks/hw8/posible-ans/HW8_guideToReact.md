
# **PRÁCTICA N° 8** 📓
**DATOS DEL ESTUDIANTE**

👨‍🎓**Docente:** Ing. Jose David Mamani Figueroa 

👨‍💻**Auxiliar:** Univ. Sergio Moises Apaza Caballero 

🎨**Materia:** DISEÑO Y PROGRAMACIÓN GRÁFICA 

📚**Siglas:** SIS-313 G1 

🗓️**Fecha:** 23/04/2024

 # 🤖 PREGUNTAS:

 👉**1. Adjunte capturas de pantalla mostrando la versión de node y de npm mediante comandos
por la terminal.** 

![node y npm version](https://github.com/sharick-14/imgenes-prc4/assets/164090219/fa647ea0-d246-4eaa-b8a8-7937e7646f56)

👉**2. Realice un video explicando lo siguiente:**

 * a. La creación de una carpeta en el disco local C:, mediante comandos desde la terminal

* b. La creación de un proyecto utilizando React con Next.js
NOTA: El video solo deberá explicar la creación de UN SOLO proyecto

    LINK DEL VIDEO ⏭️ HAGA CLICK EN EL LINK

https://drive.google.com/file/d/1VWWGHrz23aZBOHZU6irtz1rtE6z3L0uo/view?usp=sharing



👉 **3 . ¿Qué comandos se utilizó en las anteriores preguntas?**

- Para ver la version de nuestro node y del npm utlizamos el **(-v)**, en si **node  -v** y **npm -v**
 - Primero el md, para la creacion de la carpeta.
 - Utilizamos los comandos del cd para direccionarnos a la carpeta que creamos

👉 **4. ¿Qué errores tuvo al crear el proyecto? Tome en cuenta los errores que tuvo al
crear el proyecto PARA ESTA PRÁCTICA y para el LABORATORIO DEL DÍA MARTES, 16 DE
ABRIL DE 2024.
IMPORTANTE: De preferencia, adjunte las capturas de dichos errores.**

🤔 Los errores que pudimos detectar en le LABORATORIO fueron los siguientes:

😢 1 . Carpetas dobles o archivo con el mismo nombre, (osea duplicados), va a marcar un error, esto pasa cuando queremos volver a crear un proyecto o una carpeta pero con el mimsmo nombre, pero nos olvidamos borrar la carpeta anterior y nos va marcar un error o es que ya tenemos esa carpeta o archivo con el mismo nombre por lo tanto va ver duplicados y eso no acepta y por lo tanto marca **ERROR**, hay que tener cuidado en esa parte.
😢 2 . Que tengamos Antivirus pero descargados desde el navegador, y esto pasa ya que nos dice que es bueno este Antivirus(lo vemos en intenret, redes sociales), y eso nos va a perjudicar al momento de crear una proyectos React. Mejor solo tener el Antivurus del Windows solamente.
😢 3 . No direccionar bien nuestra carpeta y proyecto react, ya que nos va a marcar un error.Esto por que cuando hagamos npm run dev, debe de estar direccionado en el proyecto que creamos, si no esta direccionado de manera correcta va marcar **ERROR** **package.json**.

![package.json](https://github.com/sharick-14/imgenes-prc4/assets/164090219/8dc0faf4-7658-422c-9ebb-ff8bbf5ff145)

😢 4 . El error más comán y que nos puede pasar a todos es el tema de la mala señal de internet 📶 y esto por causa de que al momento de la descarga del proyecto, consume las megas y como estamos con megas del celular va tardar mucho y va tardar en cargar asi que  preferiblemente revisar muy bien la cantidad de megas y la velocidad del internet para evitar estos problemas, ya que nos va a perjudicar bastante.

😢 5 . Va pasar un error una vez introduzcamos el link del react, que es el siguiente
 **c:/Usuarios/<nombre>/AppData/Roaming/**

Para soluonar este problema  copiamos todo eso, hasta Roaming , en acceso directo de nuestra computadora, eso nos aparece en la parte superior como en la siguiente foto:

 ![roaming -error](https://github.com/sharick-14/imgenes-prc4/assets/164090219/af663874-64fe-4b71-8f3a-e59d1d962d1f)

Ahi debemos de crear una carpeta npm, y ya se soluciona el problema

😢 6 . El error que me paso a mi fue el de que me pedia permiso, incluso al momento de crar un a carptea me pedia permiso como administardor, era como si estuviera bloqueado y una vez que me de el pemriso recien podia crear carpetas, el problema se solo soluciona desclikeando los permisos, ya que em mi caso estaban activos, pero fue un error muy de sorpresa, ya que derreoente aparecio, y es muy poco comun, ese error.

---
🌟 *Conclusión y moraleja debemos de tener cuidado con los permisos y los diferentes errores que se nos presentaron y además a que hacemos click ya que nos va a perjudicar al momento de crear nuestro proyecto y nos quitar tiempo* 😖😫
---

👉 **5. Existen ocasiones, en las en el repositorio (gitlab), una carpeta se torna de color naranja y no se puede acceder al mismo, tal y como se muestra en la imagen.
Este mismo proyecto no permite ser clonado y en caso de éxito en la clonación, la carpeta aparece vacía.
Averigüe la causa del problema y la solución del mismo.**


El color naranja en la carpeta nos da la señal que está en un estado de conflicto, esto quiere decir que esta en conflictos con diferentes ramas o que cambios en el repositorio que perjudique a la carpeta que aparece en naranja.
Generalmente es por que hay cambios en el repositorio que aun no se han fusionado como se debe.

Los errores mas comunes, son los cambios que no sen han guardado. Por ejemplo  que no se ha realizado el commit -m.
Pero para eso  guardamos el commit -m " " y por último simpre presionamos un **push**, para que se guarde definitivamente todos los cambios y para ver que este correcto un git status.

-------
🌈 Pero este es el error por el cual pasa que la carpeta se pone naraanja: Cuando clonamos un repositorio dentro de otro va aparecer el error de la carpeta naranja ya que lacarpeta ya no se puede acceder desde el repositorio  y aparece el error de la carpeta.
y al momento de clonar un repositorio aparecera un  carpeta con el nombre de **" .git"** y la carpeta esta dentro de nuestro repositorio principal, nos va generar un error. Para solucionar ese problema debemos de borrar esa carpeta el **(. git)**, y ya luego volver a subir los cambios que hicimos con un git.push, ojo no borrar el repositorio principal sino la carpeta que esta dentro de esa.

     TAREA COMPLETA 🌟🌟🌟🌟🌟

