
#  *****PRÁCTICA N°4*****

**DATOS DEL ESTUDIANTE**

*****Docente:***** ***Ing. Jose David Mamani Figueroa***

*****Auxiliar:***** ***Univ. Sergio Moises Apaza Caballero*** 

*****Materia:***** ***DISEÑO Y PROGRAMACIÓN GRÁFICA***

*****Siglas:***** ***SIS-313 G1***

*****Fecha:***** ***26/03/2024***
                        
- [x] **Complete los niveles (Hard) Difícil y adjunte capturas de los siguientes 13 niveles vencidos:**
**10, 18, 20, 29, 30, 40, 45, 50, 60, 66, 70, 78 y 80.**
![image](https://github.com/sharick-14/imgenes-prc4/assets/164090219/f4dff4bf-db37-4c53-a536-0acc6d986f06)


- # ***Grid Attack***
![imagen de portada](https://diablodesign.eu/images/DiabloDesign/blog/Interactive-Learning/grid_attack/gridatack-1500x400.webp)

En este juego nos aventiuramos en ayudar a  Rey a encontrar a su hermano Luke

* ## ****LEVEL 10****

![rey](https://github.com/sharick-14/imgenes-prc4/assets/164090219/bc5d5c59-f648-452c-a5bb-e0c4763c3395)
En este nivel, debemos derrotar al moustro, para eso Rey, debe de lanzar un hechizo, "Malago Borno" mediante el grid-template-columns:
Pero ahora haremos uso de auto, donde tambien este puede combinar con otros valores, asiq eu haremos lo siguiente:


**#field {**

  display: grid;
  
  grid-template-columns: 1fr auto 1fr;

}

![level10](https://github.com/sharick-14/imgenes-prc4/assets/164090219/0913b1df-eae5-4c7d-9063-1720265d92db)


    Next level --->


* ## ****LEVEL 18****
![m-azazel](https://github.com/sharick-14/imgenes-prc4/assets/164090219/47e488b4-a518-428b-b9dd-b4bdc3260002)
Pra llegar a este nivel tuvimos que derrotar a varios monstrous y ahora tenemos que derrotar a Azazel,entonces Rey debe usar un hechizode **"Awakozelo!"**,y debemos de ayudarlo, para eso vamos a usar un codigo, que es el siguiente:

**#field {**
  display: grid;

  grid-template-columns:1fr 100px auto;

  grid-template-rows: repeat(2,1fr) 100px;
  
  gap:10% 20px;

}

![level18](https://github.com/sharick-14/imgenes-prc4/assets/164090219/39c1b7de-fec8-4fb3-8bf0-efe9ac0e7fb5)
  
    Next level --->


* ## ****LEVEL 20****
En este nivel vamos a poner a usar greenLand, junto con unas nuevas propiedades, que se enfoncan en las columnas, que abarca la cuadricula de una columna tanto para inicio como para final*(osea star and end)* 
Ademas de que ahora vamos a usar una o más celdas de la cuadrícula que forman un área rectangular en la cuadrícula.

**#field {**

  display: grid;

  grid-template-columns: repeat(4, 1fr);

  grid-template-rows: repeat(2, 1fr);
  
}

**#greenLand {**

  grid-column-start:2;

  grid-column-end: 4;
  
}


![level20](https://github.com/sharick-14/imgenes-prc4/assets/164090219/4b8610bf-ee38-4ba1-b758-e90324f7fd7b)

    Next level --->

* ## ****LEVEL 29****
![m-nightcall](https://github.com/sharick-14/imgenes-prc4/assets/164090219/ebdbd47a-6ecf-49df-9cb7-4672d1a303ca)
Llego el momento de demostrar lo que aprendimos durante estos niveles a **Nightcall**, ademas de uqe ya esta molesto por que ya lo  vencimos, vamos a usar greenLand y readLand, donde para greenLand vamos a us usar la siguiente:

**#greenLand {**

  grid-column: 2 / span 2;

  grid-row: 3 / 5;
} 

 y para readLand vamos a usar

 **#redLand {**

  grid-column: 4;

  grid-row: 1 / 4;

}

![level29](https://github.com/sharick-14/imgenes-prc4/assets/164090219/bd502434-9e3d-4927-9a4a-77f494350559)

    Next level --->

* ## ****LEVEL 30****
Ya logramos derrotar a nuestor segundo al segundo jefe, ene este nivel crearemos ares de cuadricula colacando elementos usando una ubicacion que se basa en lines.
Pero se puede hacer utilizando areas de cuadricula con nombre, en nuestro caso land.
Lo que debemos de hacer es que acabarque un cuadrado donde este el monstrous, por lo que estaria abarcabando 4fr para la column and row.

***NOTA:*** un punto significa una celda de cuadricula vacía.




**#field {**

  display: grid;

  grid-template-columns: repeat(4, 1fr);

  grid-template-rows: repeat(4, 1fr);

  gap: 15px;

  grid-template-areas: "land land land ."
  
  "land land land ."

  "land land land ."

  ". . . .";

}

**#redLand {**

  grid-area: land;

}


![level30](https://github.com/sharick-14/imgenes-prc4/assets/164090219/2f88e684-2d10-4c24-8e13-fafccffbab3b)

    Next level --->
* ## ****LEVEL 40****
Para este nivel vamos a hacer uso de lo aprendido de los niveles anteriores, para que podamos acabar con el moustro. Para eso nos dieron los datos de anho minimo de 150px y el ancho maximo de 1fr.
Por lo que usaremos auto-fill, lo que seria el siguiente codigo:

**#field {**

  display: grid;

  grid-template-columns: repeat(auto-fill, 150px);

  gap: 15px;

}

![level40](https://github.com/sharick-14/imgenes-prc4/assets/164090219/8035c6a6-c95d-4725-af24-c24d948970b9)

    Next level --->

* ## ****LEVEL 45****
Tenemos la presnecia de los Malignos, pero no es solo uno , son dos.
Por lo tanto se aumenta blueLand a nuestro codigo:, donde greenland, redLand, y el ultimo que se unio, estan para cada uno de los personajes que tenemos, que son los dos malignos y Rey. Por lo que se aplicara lo que aprendimos en el nivel 30 tambien.
Donde le codigo seria el siguiente:

**#field {**

  display: grid;

  gap: 15px;

  grid-template: "redLand redLand ." 100px

  "blueLand greenLand greenLand" 200px

  "blueLand greenLand greenLand" 1fr / 1fr 1fr 1fr

}

**#greenLand {**

  grid-area: greenLand;

}

**#redLand {**

  grid-area: redLand;

}


**#blueLand {**

  grid-area: blueLand;

}

![level45](https://github.com/sharick-14/imgenes-prc4/assets/164090219/f2e59aa3-57f4-4330-9512-31cc1d04da04)

    Next level --->
* ## ****LEVEL 50****
Tan solo teniamos que poner las lineas en su lugar que correspondia, ya que no estaban en su lugar, porque era una cuadrícula implícita que se trabajaba en filas, para eso solo teniamos que utlizar: ***grid-auto-rows:100px;***

Por lo que el codigo completo es el siguiente:

**#field {**

  display: grid;

  gap: 20px;

  grid-template: 1fr 1fr / 1fr 1fr;

  grid-auto-rows: 100px;
  
}

![level50](https://github.com/sharick-14/imgenes-prc4/assets/164090219/df0163bf-b9cc-456e-82d3-99e7f7778cac)
 
    Next level --->
* ## ****LEVEL 60****
 Para este siguiente nivel, aplicamos una nueva propiedad, que es ***place-items*** que se establece para align-items y para justify-items:
 Por lo que el codigo estaria de la siguiente forma:

 **#field {**

  display: grid;

  gap: 15px;

  grid-template-columns: repeat(3, 1fr);

  place-items: center end;

  
}

Estariamos estableciendo en place-items: center  y end.

![level60](https://github.com/sharick-14/imgenes-prc4/assets/164090219/ebd4b228-187f-4175-b38d-9996c6f2961c)

    Next level --->

* ## ****LEVEL 66****
![zarax](https://github.com/sharick-14/imgenes-prc4/assets/164090219/18031c0c-1ab8-432a-bc5e-a87a5f46b8c5)
En este nivel tenemos que derrotar a **Zarax**, que nos menos precia, por un elfo pequeño, asiq ue tenemosque demostrale que esta equivocado.
Asi que para eso utlizaremos  justify-items y aling-items, esto para que ponamos ajustarlo, esto en field, redLand, y greenLand, donde en el ptimro estariamos ajustandolo en center, en readLand en end, y greenLand en start y end.

**#field {**

  display: grid;

  gap: 15px;

  grid-template: 1fr 1fr / 2fr 1fr;

  justify-items: center;

  align-items: center;

  
}

**#field > div {**  

  height: 50%;

  width: 50%;

  
}

**#redLand {**

  justify-self: end;

  align-self: end;

  
}

**#greenLand {**

  justify-self: start;

  align-self: end;

}

![level66](https://github.com/sharick-14/imgenes-prc4/assets/164090219/4dfd38a4-7409-4e93-b226-b23d98780bff)

    Next level --->

* ## ****LEVEL 70****
Para este nivel combinaremos el justify-content y justify-items, con elemento que ya utlizamos en el anterior juego, que son space-evenly y end; ademas estariamos aplicando grid-template: 1fr 1fr/40% 40%;
Por lo que el codigo quedaria de la siguiente manera: 

**#field {**

  display: grid;

  grid-template: 1fr 1fr/40% 40%;

  justify-content: space-evenly;

  justify-items: end;

  
}

**#field > div {**

  width: 50%;

  height: 50%;

  
}

![level70](https://github.com/sharick-14/imgenes-prc4/assets/164090219/10c668d1-8f09-4286-ab5d-54107d428787)

    Next level --->
* ## ****LEVEL 78****
![m-valcorian](https://github.com/sharick-14/imgenes-prc4/assets/164090219/004d38c6-d67f-4a9b-9e31-b9ba9b3000ea)
ohh no!!! Nos encontramos a **Valcoriano** y como siempre nos subestiman, asi que usaremos nuestra arma secreta. Usaremos lo aprendido en los anteriores niveles  como el **gap, grid-template, grid-area**. 

**#field {**

  display: grid;

  gap: 15px;

  grid-template: "greenLand . blueLand" 1fr

  "greenLand redLand ." 1fr

  / 100px 1fr 1fr;

}

**#greenLand {**

  grid-area: greenLand;

}

**#redLand {**

  grid-area: redLand;

}

**#blueLand {**

  grid-area: blueLand;

  height: 50%;

  align-self: center;

}

![level78](https://github.com/sharick-14/imgenes-prc4/assets/164090219/a6fa5b90-b639-4bce-a7e3-862f17174d04)

    Next level --->
* ## ****LEVEL 80****
![luke](https://github.com/sharick-14/imgenes-prc4/assets/164090219/d471d9c8-4b86-4891-ad72-6af5f04e1455)
Finalmente, encontramos  **Luke**, pero **Valcorian** hizo de las suyas, sino resolvemos bien el rompecabezas morira Rey, que debemos de ayudar a Rey a salvar a su hermano.

Por lo que el codigo, que nos ayduara a salvar a su hermano es el siguiente:

**#field {**

  display: grid;

  grid: repeat(3, 1fr);

}


**#greenLand {**

  grid-column: 1 / 3;

  grid-row: 1;

}


**#redLand {**

  grid-column: 2 / 4;

  grid-row: 1 / 3;


}
**#blueLand {**

  grid-column: 2;

  grid-row: 1 / 5;

}

![level80](https://github.com/sharick-14/imgenes-prc4/assets/164090219/cfd78e13-0a62-4ee5-bb87-cb3d337d6f4b)

    Nivel completado --->

    GRACIAS