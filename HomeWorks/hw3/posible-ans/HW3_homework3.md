
# **PRÁCTICA N°3**

**DATOS DEL ESTUDIANTE**
****Materia :**** **DISEÑO Y PROGRAMACIÓN GRÁFICA**

****Siglas:**** **SIS-313 G1**

****Fecha:**** **19/03/2024**

---->*Intrucciones del juego* **FLEX BOX ADVENTURE**<----

    *NOTA : Para las intrucciones y la realización de cada nivel, son las mismas para el nivel medio y dificil.


*****Nivel 1*****  
En el nivel 1, hay ayudar a Arthur, para eso Arthur debe de comer la manzana y vamos a utilizar el justify-content:center;


*****Nivel 2*****   
En el nivel 2, lo que debemos de hacer es que Arthur se pueda comer su la zanahoria para recuperar sus fuerzas, para eso necesitamos que Arthur vaya al otro extremo, donde esta la zanahoria, para eso utlizaremos el justify-content:flex-end;  

*****Nivel 3*****   
Para este nivel lo que debemos de hacer ayudar a la amiga de Arthur, que es Marilyn a recuperar su fuerza, pero las comidas uqe necesitan ambos, estan a los extremos y para eso utilizaremos justify-content: space-between;
Esto nos ayudara a que ambos personajes se separen en los extremos diferentes, por eso se pone el space-between.

*****Nivel 4***** 
Para este siguiente nivel necesitamos que ambos personajes deben de comer su comida, ya que aún les falta para que lleguen al 100 %, pero la comida esta separada, por lo tanto utilizaremos el justify-content:space-around;

*****Nivel 5*****  
En este nivel aun no llegaron al 100 % de salud, por lo tanto deben de seguir comiendo comida, ambas comidas estan separadas, los personajes deben de estar en la misma posición, por lo tanto utlizaremos un justify-content:space evenly;

*****Nivel 6*****     
En este nivel lo que haremos es que los tres personajes incluido el nuevo Haymitch luchen contra los enemigos, para eso utilizaremos el align-items:center;
esto para que los personajes, todos al mismo al mismo tiempo y juntos se muevan al centro.

*****Nivel 7*****  
En este nivel lo que haremos sera que los tres personajes se desplazen a la parte  inferior donde estan los enemigos y utlizaremos el align-items:flex-end;

*****Nivel 8*****    
Para este nivel los enemigos estar alineados en el centro, por lo tanto necesitamos qeu nuestros personajes se muevan juntos al centro, asi que utilizaremos el align-items: center;   sin embargo aun los personajes estan en un costado asi que los ajustaremos, utlizando el justify-content:center:, para que se los personajes se ajusten al centro

*****Nivel 9*****     
Para este nivel necesitamos derrotar a los duendes, los personajes estan en un extremo ala izquierda, pero los duendes estan en la parte inferior, pero separados, asi que utlizaremos el align-items:flex-end; esto para que se muevan a la parte final osea la inferior, pero los persoanjes estan juntos y neceitams que se separen un poco, asi que utlizaremos el justify-content:space-between; 
esto para que los personajes se separen con mucho mas espacio 

*****Nivel 10*****  
Para este nivel necesitamos derrotar a los enemigos, pero estos estan en una posición de columna y para que eso utlizaremos el flex-direction:column;
esto para que a los presonajes se los direccione en columna

*****Nivel 11*****   
Para este nivel lo que debemos de hacer es que los personajes se muevan en todos a al otro extremo en fila, todos juntos, pero segun las intrucciones cada personaje tiene asignado un enemigo, y para eso utlizaremos el flex-direction:row-reverse;

*****Nivel 12***** 
Para este siguiente nivel los persoanjes estan en fila en un extremo superior, pero los enemigos estan en columna, pero en la parte inferior, asi que utlizaremos flex-direction:column; pero tenemos que ajustar a los personajes para que vayan hacia la aprte inferior por lo tanto urlizaremos el justify-content:flex-end; 

*****Nivel 13***** 
En este nivel ya aparecio el jefe Pluto, y tenemos que derrotarlo junto a los enemigos que lo acompañan, pero los personajes estan en una fila a un extremo a la izquierda, y los personajes al centro, por lo tanto utlizaremos el felx-direction: row-reverse; esto por que cada personaje tiene asignado un enemigo y quiero que se mueva en forma de fila, leugo utlizaremos el justify-content: center; para ajustarlos y que se centren, pir ultimo utilizaremos el align-items:center para que se alineen con los enemigos

*****Nivel 14***** 
En este nivel Arthur perdio un poco de salud, asi que deben de comer, y lo que haremos sera que Arthur se ponga en el orden 2, asiendo lo siguiente,   order: 2;

*****Nivel 15***** 
Para este siguiente nivel debemos de enfrentarnos a Zelus un nuevo jefe, solo Marilyn debe de combatirlo, asi que utlizaremos el align-self, esto para que los demas personakes no se muevan de su puesto y solo Marilyn  se mueva, en este caso el enemigo esta al centro, asi que haremos lo siguiente:
align-self: center;

*****Nivel 16***** 
Para  este nivel solo Haymitch luvhe contra el fantasma, sin embargo el fantasmaesta en un extremo inferior, asi que utlizaremos el align-self:flex-end;
y que este en el el order: 1;

*****Nivel 17***** 
Para este siguiente nivel lo que haremos sera que los personajes luchen contra los tres enemigos, pero cada heroe tinee que estar con un enemigo, asi que utlizaremos el Flex-direction:column-reverse; ya que los personajes estan en fila, y necesitamso que se muevan en columna, cambiando de posicion, luego utlizaremos el justify-content: flex-end;  y por ultimo el align-items:center; para que todos los personajes se pongan al centro.
*****Nivel 18*****

Para combatir a Zelus Arthur debe de combatirle utilizando el align-self, que sirve unicamente para Arthur, este debe estar al center para combatir contra Zelus, luego utlizamos el order, para que avance en la posicion 2.

*****Nivel 19*****

En este nivel debemos de poner las monedas a los cofres y para eso debemos de utilizar el Flex-wrap:wrap, donde luego debemos de poner tres monedas a los tres cofres de abajo.Ya que en la fila de las monedas, estan 8 monedas, y abajo estan tres cofres, y solo queremos que tres monedas se muevan por eso utlizaremos el wrap


*****Nivel 20*****

En este nivel las monedas deben estar al centro ya que ahi en el centro estan los cofres donde deben de introducirse las monedas-para esto utlizaremos el align-content: center, para alinear las monedas al centro

*****Nivel 21*****

Para este nivel queremos que las monedas se muevan a la parte inferior en este caso 5 a los 5 cofres, y los otros tres que esten al centro en los 3 cofres, para esto utlizaremos el align-content: flex-end; para que bajen a la parte inferior y luego el justify content: center, para que las tres monedas se ajusten al centro.

*****Nivel 22*****

Par este nivel nos enfrentamos a Cronos y sus sirvientes, asi que nuestros heroes deben de combatirlo, para esto los personajes deben de estar en columna pero cambiados de orden , ya que cada heroe tiene un enemigo, asiq eu utlizaremos el flex-direction: column-reverse; , luego debemos de ajustarlos dando un espacio entre cada heroe, para eso utlizaremos el justify-content: space-arund;  por ultimo, debemos de alinerarlos, nos fijamos que los heroes estan en el otr extremo, en si en la derecha, para eso utlizaremos el align-items: flex-end;

*****Nivel 23*****

Esta es la penultima vida que le queda a Cronos asi que debemos de atacarlo  de la siguiente manera, primero debemos de colocar a cada heroe con cada enemigo , y tambien las monedas en su respectivos cofres, por lo tanto utlizaremos lo siguiente: flex-direction: column-reverse, par qeu los heroes que etsna en fila se cambien a columna y ques los heroes cambien de orden, luego utlizaremos justify-content:center;   luego el flex-wrap: wrap-reverse;   y finalmente el align-content: center;, esto para qeu se puedan centrar todos los heroes

*****Nivel 24*****

Finalmente llegamos al final en este nos enfrentaremos a Cronos que ya sol tiene esta vida, para derrotarlo simpelmente tenemos hacer que solo Arthur se mueva al centro, asi que utlizaremos align-self:center;   y por ultimo en Arthur se debe de posicionar en el 3 u order:3; con esto derrotamos a Cronos.


----------------------------------------------------------------
*****MEDIUM LEVEL*****

*****Nivel 21*****   

![level21](https://github.com/sharick-14/imagens-practica3/assets/164090219/a61b7437-5de9-46d1-b522-d62ecc1c1009)


*****Nivel 22***** 

![level22](https://github.com/sharick-14/imagen/assets/164090219/a83e888f-950c-4bc2-9fd8-71fef71ba9b2)



*****Nivel 23***** 

![level23](https://github.com/sharick-14/imagen/assets/164090219/ab60a5e0-f99a-46d4-808f-796a646cf66f)



*****Nivel 24*****

![level24](https://github.com/sharick-14/imagen/assets/164090219/9e3329f6-137e-48bc-8ad5-92a3b9e76ccb)


----------------------------------------------------------------
*****HARD LEVEL*****

*****Nivel 21*****   


![level21-dif](https://github.com/sharick-14/imagen/assets/164090219/41a43b27-cf5e-4e40-9cf6-66afad59b1c4)

*****Nivel 22***** 


![level22-dif](https://github.com/sharick-14/imagen/assets/164090219/03d0baf4-a501-4686-b13b-5bf422a8acab)


*****Nivel 23***** 
![level23-dif](https://github.com/sharick-14/imagen/assets/164090219/b5525176-61dc-4246-81f9-d69025331e7d)


*****Nivel 24*****

![level24-dif](https://github.com/sharick-14/imagen/assets/164090219/58250b92-f99b-4829-a44c-b6063059bae4)









































