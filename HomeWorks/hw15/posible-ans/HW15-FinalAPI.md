
# 🍂🐦‍🔥 **PRÁCTICA N° 15** 🐦‍🔥🍂
**DATOS DEL ESTUDIANTE**

🍁👨‍🎓**Docente:** Ing. Jose David Mamani Figueroa 🍁

🍁👨‍💻**Auxiliar:** Univ. Sergio Moises Apaza Caballero 🍁

🍁🎨**Materia:** DISEÑO Y PROGRAMACIÓN GRÁFICA🍁

🍁📚**Siglas:** SIS-313 G1 🍁

🍁🗓️**Fecha:** 21/06/2024 🍁

🟪 *PREGUNTAS:*
## 1. Realizar el consumo de una API en la página web creada por ustedes (la cual se encuentra en el repositorio React-with-<ApellidosNombres>). (50%)
Las APIs elegidas para la tarea son The Cat Api (url: https://thecatapi.com) y The
Dog Api (url: https://thedogapi.com); se puede trabajar con cualquiera, a su libre
elección.
Se deben seguir los siguientes criterios:

• Mostrar por lo menos 20 fotos (en la documentación indica cómo)

• ¡IMPORTANTE! Usar algún botón o elemento en su página para que pueda
consumir los datos (especificar en el archivo README cual es este elemento).

• Al presionar el elemento (botón, link, etc) aparecerán las imágenes debajo
del elemento (nav, header, sección o donde se encuentre su elemento).
Se tomarán en cuenta los siguientes criterios de calificación:

Consumo de API : 80%

Diseño : 20%
___________________________
TOTAL : 100%

## 2. Investigar los mensajes de GET, POST y PUT ¿Qué son? ¿Para qué sirven?

☑️ **GET**: Solicita una representación de un recurso. Se utiliza para recuperar datos y no tiene efectos secundarios en el servidor. Los datos se envían a través de la URL y son visibles en la barra de direcciones del navegador.
Por ejemplo, cuando visitas una página web en un navegador, estás haciendo una solicitud GET al servidor para recuperar y mostrar la página web solicitada.

🪧**GET: El recurso se ha obtenido y se transmite en el cuerpo del mensaje.**

☑️**POST**: Envía una entidad (datos) a un recurso específico. A menudo, provoca un cambio en el estado o efectos secundarios en el servidor. Los datos se envían en el cuerpo de la solicitud HTTP y no son visibles en la URL.Además, la capacidad del método POST es ilimitada. Por ejemplo, al enviar un formulario web con datos como nombre de usuario y contraseña, estos datos se envían al servidor utilizando el método POST.

☑️**PUT**: Reemplaza todas las representaciones actuales del recurso de destino con la carga útil de la petición. Se utiliza para actualizar o crear recursos en el servidor. Se utiliza para enviar datos al servidor para actualizar o modificar un recurso existente en una ubicación específica. Es comúnmente utilizado en aplicaciones web y APIs RESTful para realizar actualizaciones de datos. A diferencia de POST, PUT suele ser idempotente, lo que significa que realizar la misma operación varias veces no tiene efectos adicionales más allá de la primera vez.

🪧**PUT o POST: El recurso que describe el resultado de la acción se transmite en el cuerpo del mensaje.**

![image](https://github.com/sharick-14/imgenes-prc4/assets/164090219/92ab5cf7-63b7-4936-be51-fb5a6e9a46ba)

- **GET**: Recuperar datos identificados por la URL.
- **POST**: Enviar para ser procesados o almacenados.
- **PUT**: Actualizar datos de un recurso en el servidor.


## 3. ¿Para qué sirven los códigos de respuesta HTTP?

![download](https://github.com/sharick-14/imgenes-prc4/assets/164090219/0fa61ae0-6110-41a2-9382-574c36211450) 

Un código de estado HTTP es un mensaje que el servidor de un sitio enví­a al navegador para indicar si una solicitud puede ser cumplida o no.
Los códigos de estado son marcados por la W3C. Estos son embebidos en el encabezado HTTP de una página para decirle al navegador el resultado de su solicitud. Cuando todo sale bien, el servidor devuelve un código 200. Sin embargo, hay muchas cosas que podrí­an salir mal al tratar de cumplir una solicitud de un navegador para un servidor.

![download](https://github.com/sharick-14/imgenes-prc4/assets/164090219/504f7491-38fc-4f86-9a05-061e589e38f1)

Estos códigos de respuesta son importantes porque permiten al cliente y al servidor entender y actuar en consecuencia según lo que sucede con una solicitud específica.

![a163caf6-c2b7-4a75-88c7-321878d85bf9](https://github.com/sharick-14/imgenes-prc4/assets/164090219/8790ff9a-c965-4dc2-872c-b85d09635e46)

## 4. Los códigos de respuesta HTTP se agrupan en 5 clases, investigue cuales son y que números abarcan.

Los códigos de estado de respuesta HTTP indican si se ha completado satisfactoriamente una solicitud HTTP específica. Las respuestas se agrupan en cinco clases:

    1. Respuestas informativas (100–199)
       Esta clase de códigos de respuesta se utiliza para fines informativos y no tienen un impacto directo en la solicitud.
    2. Respuestas satisfactorias - Exitosas (200–299)
       Indican que la solicitud fue recibida, entendida y aceptada exitosamente por el servidor.
    3. Redirecciones (300–399)
       Estos códigos indican que el cliente necesita realizar una acción adicional para completar la solicitud.
    4. Errores de los clientes (400–499)
       Indican errores causados por la solicitud del cliente, como sintaxis incorrecta, autorización insuficiente, etc.
    5. Errores de los servidores (500–599).
       Indican que hubo un error en el servidor al procesar la solicitud del cliente, generalmente debido a problemas con el servidor.
    


![image](https://github.com/sharick-14/imgenes-prc4/assets/164090219/95f1da89-e409-43dd-a8a2-c8bfc78e6bd3)


## 5. Investigar los siguientes códigos de respuesta HTTP, mencione el nombre del código y explique que indica el mismo:
![image](https://github.com/sharick-14/imgenes-prc4/assets/164090219/b80a4f5f-4b66-4f5a-8e22-2ef4c540422a)

🟠*a. 200*

- **200 OK**
Indica que la solicitud ha tenido éxito. El servidor ha recibido y procesado la solicitud sin problemas y está devolviendo el recurso solicitado.


🟠*b. 400*
- **400 Bad Request**
Indica que la solicitud contenía sintaxis incorrecta o no pudo ser entendida por el servidor. Es decir, hay un problema con la solicitud que impide que el servidor la procese.

🟠*c. 401*
- **401 Unauthorized**
Indica que se requiere autenticación para acceder al recurso solicitado. El cliente debe proporcionar credenciales válidas (como nombre de usuario y contraseña) para obtener acceso.

🟠*d. 403*
- **403 Forbidden**
Indica que el servidor entendió la solicitud del cliente, pero se niega a cumplirla. Esto puede ser debido a la falta de permisos para acceder al recurso o a políticas de seguridad establecidas por el servidor.

🟠*e. 404*
- **404 Not Found**
Indica que el servidor no pudo encontrar el recurso solicitado. Este es uno de los errores más comunes que se encuentran al navegar por la web, generalmente debido a URLs incorrectas o recursos que han sido eliminados o movidos sin redireccionamiento.

Los vínculos que conducen a una página 404 son normalmente llamados vínculos rotos o vínculos muertos , y pueden estar sujetos a Enlace Roto. Un código de estado 404 no indica si el recurso está temporal o permanentemente ausente

🟠*f. 408*
- **408 Request Timeout**
Indica que el servidor ha cerrado la conexión debido a que el cliente no envió una solicitud completa dentro del tiempo esperado por el servidor.

Esta respuesta se usa mucho más desde que algunos navegadores, como Chrome, Firefox 27+, e IE9, usan el mecanismo de pre-conexión HTTP para acelerar la navegación.

🟠*g. 500*
- **500 Internal Server Error**
Indica un error genérico en el servidor que impide que procese la solicitud del cliente. Este código es utilizado cuando no se puede determinar la causa exacta del error.

Esta respuesta de error es una respuesta genérica "atrapa todo". Por lo general, esto indica que el servidor no puede encontrar un mejor código de error 5xx para responder. A veces, los administradores del servidor registran respuestas de error como el código de estado 500 con más detalles sobre la solicitud para evitar que el error vuelva a ocurrir en el futuro.

🟠*h. 501*
- **501 Not Implemented**
Indica que el servidor no soporta la funcionalidad necesaria para cumplir con la solicitud del cliente. Es decir, el servidor no reconoce el método de solicitud o no puede cumplir con la solicitud por alguna razón.
Los únicos métodos que los servidores deben admitir (y por lo tanto no deben devolver 501) son GETy HEAD.

🟠*i. 502*
- **502 Bad Gateway**
Indica que el servidor, mientras actuaba como puerta de enlace o proxy, recibió una respuesta no válida desde el servidor ascendente al que accedió para cumplir la solicitud.

    ⭕Nota: Una puerta de enlace puede referirse a cosas distintas en redes y un error 502 no es algo que normalmente puedas arreglar, ya que requiere correcciones por parte del servidor o los proxies a través de los que intentas acceder.

🟠*j. 504*
 - **504 Gateway Timeout**
Indica que el servidor, actuando como puerta de enlace o proxy, no pudo recibir una respuesta a tiempo desde el servidor ascendente al que accedió para cumplir la solicitud.

## 6. ¿Qué es un endpoint?

![image](https://github.com/sharick-14/imgenes-prc4/assets/164090219/eeebf608-c172-42f6-a1bc-b6ff8067804d)

Un endpoint es cualquier dispositivo que se conecta físicamente a la red y al que ésta puede acceder. Los endpoints son los componentes situados en el extremo de un canal de comunicación con la red y se utilizan para intercambiar datos de ida y vuelta con ella.

Los endpoints se utilizan para comunicar información hacia y desde una red. Son dispositivos que permiten a los usuarios introducir, recibir o manipular información. Sin endpoints, los usuarios no tendrían forma directa de comunicarse con la red o acceder a datos de ella.

En el contexto de las aplicaciones y servicios web, un "endpoint" se refiere a un punto final o una URL específica a la que se puede acceder para realizar operaciones o interactuar con un servicio. Es decir, un endpoint es simplemente una dirección única (URL) a la que se envían solicitudes y desde la cual se reciben respuestas.

Los endpoints son parte fundamental de las APIs (Interfaces de Programación de Aplicaciones) y de los servicios web en general. Cada endpoint está asociado a una funcionalidad específica del servicio o la API a la que está conectado. Por ejemplo, un servicio de gestión de usuarios podría tener endpoints para crear usuarios, obtener detalles de usuarios, actualizar información, eliminar usuarios, etc.