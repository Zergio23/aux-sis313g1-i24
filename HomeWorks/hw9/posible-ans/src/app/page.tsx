'use client';

import Image from "next/image";
import styles from "./page.module.css";
import Button from "@/components/Button";
import Pokemon from "@/components/Pokemon";
import Video from "@/components/Video";
import Plan from "@/components/Plan";

export default function Home() {
  return (
    // aca no
    <main className={styles.main}>
      <div className="p1">
        <h1>PREGUNTA 1: BOTONES</h1>
        <div>
          <Button text="hola"/>
          <Button text="adios" transparent/>
          <Button text="test"/>
          <Button text="test" transparent/>
          <Button text="funciona"/>
          <Button text="wiii" transparent/>
        </div>
      </div>
      <div className="p2">
      <h1>PREGUNTA 2: POKÉMON</h1>
      <div>
        <Pokemon name="Psyduck" type="Water" imageUrl="./img/psyduck.jpg"/>
        <Pokemon name="Wigglytuff" type="Normal/Fairy" imageUrl="./img/wigglytuff.jpg"/>
        <Pokemon name="Snorlax" type="Normal" imageUrl="./img/snorlax.jpg"/>
      </div>
      </div>
      <div className="p3">
        <h1>PREGUNTA 3: VÍDEO</h1>
        <div>
          <Video title="El principio y el final" author="La Logia" videoUrl="./video/el-principio-logia.mp4"/>
          <Video title="Feel High" author="Loukass" videoUrl="./video/feel-high-loukass.mp4"/>
          <Video title="Clausura" author="Quirquiña" videoUrl="./video/clausura-quirquina.mp4"/>
        </div>
      </div>
      <div className="p4">
        <h1>PREGUNTA 4: PLANES</h1>
        <div>
          <Plan plan="Free" price="10" imageUrl="./img/logo.svg"/>
          <Plan plan="Standard" price="10" imageUrl="./img/banner-img.svg"/>
          <Plan plan="Premium" price="10" imageUrl="./img/plan-img.svg"/>
        </div>
      </div>
    </main>
  );
}
