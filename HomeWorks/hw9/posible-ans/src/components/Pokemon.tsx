export default function Pokemon(props: any) {
    return(
        <div className="poke">
            <h1>{props.name}</h1>
            <img src={props.imageUrl} alt="poke" />
            <p>Type: {props.type}</p>
        </div>
    )
}