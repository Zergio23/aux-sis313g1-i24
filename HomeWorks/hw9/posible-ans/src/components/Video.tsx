export default function Video(props: any) {
    return(
        <div className="video">
            <h1>{props.title}</h1>
            <p>Autor: {props.author}</p>
            <video controls>
                <source src={props.videoUrl}/>
            </video>
        </div>
    )
}