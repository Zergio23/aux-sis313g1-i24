export default function Plan(props: any) {
    return(
        <div className="plan">
            <div>
                <img src={props.imageUrl} alt="" />
                <h2>{props.plan} Plan</h2>
                <ul>
                    <li><img src="./img/check.svg" alt="" />Unlimited Bandwitch</li>
                    <li><img src="./img/check.svg" alt="" />Encrypted Connection</li>
                    <li><img src="./img/check.svg" alt="" />No Traffic Logs</li>
                    <li><img src="./img/check.svg" alt="" />Works on All Devices</li>
                </ul>
            </div>
            <div>
                <h1>${props.price} <span>/ mo</span></h1>
                <button>Select</button>
            </div>
        </div>
    )
}