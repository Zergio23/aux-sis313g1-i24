export default function Button(props: any) {
    return(
        props.transparent?
            <button className="btn-transparent">{props.text}</button>
        :

            <button className="btn-style">{props.text}</button>
    )
}