# DISEÑO Y PROGRAMACIÓN GRÁFICA G1
Bienvenido/a al repositorio de la auxiliatura de la materia **SIS-313**.  
**DOCENTE:** Ing. Jose David Mamani Figueroa  
**AUXILIAR:** Univ. Sergio Moises Apaza Caballero  
**GESTIÓN:** I/2024  
**GRUPO:** G1  

> Link del repositorio de auxiliatura de React:  
> [REACT REPOSITORY](https://gitlab.com/Zergio23/react-with-sis313aux.git)

# HORARIO DE CLASES
| DÍA     | HORA          | AMBIENTE   |
| :-----: | :-----------: | :--------: |
| LUNES   | 16:15 - 18:30 | 3ra. Amb 4 |
| VIERNES | 16:15 - 18:30 | 1ra. LAB 2 |

## SUMARIO
1. [DISEÑO Y PROGRAMACIÓN GRÁFICA G1](#diseño-y-programación-gráfica-g1)
    - [HORARIO DE CLASES](#horario-de-clases)
    - [SUMARIO](#sumario)

2. [PRÁCTICAS](#prácticas)
    - [CALIFICACIONES](#calificaciones)
    - [TOP](#top)
    - [PRÁCTICAS](#prácticas)
    - [PRÁCTICAS ENTREGADAS](#prácticas-entregadas)
    - [PRÁCTICAS REVISADAS](#prácticas-revisadas)

3. [EXÁMENES](#exámenes)
    - [EXÁMENES RESUELTOS](#exámenes-resueltos)
    - [DIRECTORIOS](#directorios)

4. [LABORATORIO](#laboratorios)

5. [MATERIAL ELABORADO POR EL AUX](#material-elaborado-por-el-aux)

6. [MATERIAL](#material)
    - [README.MD](#readme.md)
    - [PC](#pc)
    - [ANDROID](#android)
    - [FIGMA](#figma)
    - [PÁGINAS PARA APRENDER FLEX Y GRID](#páginas-para-aprender-grid-y-flex)
    - [ÍCONOS](#iconos)
    - [PÁGINA PARA FUENTES DE TEXTO](#página-para-fuentes-de-texto)
    - [NODE.JS](#nodejs)

7. [DOCUMENTACIÓN](#documentación)
    - [DOCUMENTACIÓN GENERAL](#documentación-general)

# PRÁCTICAS
## CALIFICACIONES 
[Ver Calificaciones](/score/califications/README.md)

## TOP
[Ver PODIO/TOP](/score/top/README.md)

## PRÁCTICAS
[Práctica Xtra file](/HomeWorks/xtra/prácticaN0_auxSIS313G1.pdf)  
[Práctica 1 file](/HomeWorks/hw1/prácticaN1_auxSIS313G1_corregido.pdf)  
[Práctica 2 file](/HomeWorks/hw2/prácticaN2_auxSIS313G1.pdf)  
[Práctica 3 file](/HomeWorks/hw3/prácticaN3_auxSIS313G1.pdf)  
[Práctica 4 file](/HomeWorks/hw4/prácticaN4_auxSIS313G1.pdf)  
[Práctica 5 file](/HomeWorks/hw5/prácticaN5_auxSIS313G1.pdf)  
[Práctica 6 file](/HomeWorks/hw6/prácticaN6_auxSIS313G1.pdf)  
[Práctica 7 file](/HomeWorks/hw7/prácticaN7_auxSIS313G1.pdf)  
[Práctica 8 file](/HomeWorks/hw8/prácticaN8_auxSIS313G1.pdf)  
[Práctica 9 file](/HomeWorks/hw9/prácticaN9_auxSIS313G1.pdf)  
[Práctica 10 file](/HomeWorks/hw10/prácticaN10_auxSIS313G1.pdf)  
[Práctica 11 file](/HomeWorks/hw11/prácticaN11_auxSIS313G1.pdf)  
[Práctica 12 file](/HomeWorks/hw12/prácticaN12_auxSIS313G1.pdf)  
[Práctica 13 file](/HomeWorks/hw13/prácticaN13_auxSIS313G1.pdf)  
[Práctica 14 file](/HomeWorks/hw14/prácticaN14_auxSIS313G1.pdf)  
[Práctica 15 file](/HomeWorks/hw15/prácticaN15_auxSIS313G1.pdf)  

## PRÁCTICAS ENTREGADAS
- [X]  Práctica aux
- [X]  Práctica 1
- [X]  Práctica 2
- [X]  Práctica 3
- [X]  Práctica 4
- [X]  Práctica 5
- [X]  Práctica 6
- [X]  Práctica 7
- [X]  Práctica 8
- [X]  Práctica 9
- [X]  Práctica 10
- [X]  Práctica 11
- [X]  Práctica 12
- [X]  Práctica 13
- [X]  Práctica 14
- [X]  Práctica 15
- [X]  Práctica 16

## PRÁCTICAS REVISADAS
- [X]  Práctica aux
- [X]  Práctica 1
- [X]  Práctica 2
- [X]  Práctica 3
- [X]  Práctica 4
- [X]  Práctica 5
- [X]  Práctica 6
- [X]  Práctica 7
- [X]  Práctica 8
- [X]  Práctica 9
- [X]  Práctica 10
- [X]  Práctica 11
- [X]  Práctica 12
- [X]  Práctica 13
- [X]  Práctica 14
- [X]  Práctica 15
- [X]  Práctica 16

# EXÁMENES
## EXÁMENES RESUELTOS
- [X]  Parcial 1
- [X]  Parcial 2
- [X]  Parcial 3
- [ ]  Final

## DIRECTORIOS
[Parcial 1 files](/ExamSolutions/parcial1/)  
[Parcial 2 files](/ExamSolutions/parcial2/)  
[Parcial 3 files](/ExamSolutions/parcial3/)  
**Final files:**  AÚN NO DISPONIBLE  

# LABORATORIOS
[Lab. 2 - 26/mar/2024](/Labs/lab2-260324/)  
[Lab. 3 - 02/apr/2024](/Labs/lab3-020424/)  
[Lab. 4 - 04/jun/2024](/Labs/lab4-040624/)  
[Lab. 5 - 11/jun/2024](/Labs/lab4-040624/)  
> Se usará el mismo archivo del lab 4 para el lab 5  


# MATERIAL ELABORADO POR EL AUX
**Creando un repositorio/proyecto GitLab**  
[Creando un repositorio/proyecto GitLab](/extra/Creating%20a%20GitLab%20Project.pdf)

**Creación de archivos README.MD con imágenes**  
[Creación de archivos README.MD con imágenes](/extra/Creación%20de%20archivos%20README.md%20con%20imágenes.pdf)

**Subiendo archivos al repositorio con Git**  
[Subiendo archivos al repositorio con Git](/extra/Subiendo%20archivos%20al%20repositorio%20con%20Git.pdf)

**Creando proyecto de React con Next.js**  
[Creando proyecto en React con Next](/extra/Creando%20proyecto%20en%20React%20con%20next.pdf)

# MATERIAL
## README.MD
**Página para crear archivos README.md**  
https://readme.so/es

## PC
**Visual Studio Code**  
https://code.visualstudio.com/

**Git Terminal**  
https://git-scm.com/downloads

## ANDROID
**ACode (Editor de texto para android)**  
https://play.google.com/store/apps/details?id=com.foxdebug.acodefree

**Termux (Terminal para android)**  
https://termux.dev/en/

## FIGMA
https://www.figma.com/

## APIs
https://www.postman.com

## MEJORAR LA ESTÉTICA DE LA TERMINAL (POWERSHELL)
https://ohmyposh.dev

## PÁGINAS PARA APRENDER FLEX Y GRID
**Flex Box Adventures**  
https://codingfantasy.com/games/flexboxadventure

**Grid Attack**  
https://codingfantasy.com/games/css-grid-attack

**Css Diner**  
https://flukeout.github.io/

## ÍCONOS
**Font Awesome**  
https://fontawesome.com/

**Feather Icons**  
https://feathericons.com/

## NODE.JS
**Node.js**  
https://nodejs.org/en  
**Versión:**  
**node.js:** 20.12.2  
**npm:** 10.5.0  

## PÁGINA PARA FUENTES DE TEXTO
https://fonts.google.com/

# DOCUMENTACIÓN
## DOCUMENTACIÓN GENERAL
**Documentación archivos README.md**  
https://docs.github.com/es/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax

***
**Documentación HTML5**  
https://developer.mozilla.org/en-US/docs/Web/HTML

**Documentación CSS**  
https://developer.mozilla.org/en-US/docs/Web/CSS

**Documentación JAVASCRIPT**  
https://developer.mozilla.org/en-US/docs/Web/JavaScript

***
**Documentación React**  
https://es.react.dev/reference/react#react

**Documentación Next**  
https://nextjs.org/docs

**Documentación MaterialUI**  
https://mui.com/material-ui/getting-started/installation/